let taskApp = angular.module("taskApp",["ngRoute","registerModule"]);
let registerModule = angular.module("registerModule",[]);

taskApp.config(function($routeProvider) {
    this.check_token = function($location, $http){
      let token = localStorage.getItem('UserToken');
      if(token!=null){
        $http({
          method: 'POST',
          url: 'http://127.0.0.1:8000/api/auth/me',
          headers: {
             'Authorization': 'Bearer '+token
          },
        }).then(function successCallback(response) {
          //alert("da")
        },
        function errorCallback(response) {
            localStorage.removeItem('UserToken');
            $location.path('/login');
          });
      }
      else{
        $location.path('/login');
      }
    };

    $routeProvider
    .when("/", {
         template : "<tasks-list></tasks-list>",
         resolve:{
            "check":function($location, $http){this.check_token($location, $http)}
        }
    })
    .when('/details/:taskId', {
          template: '<task-details></task-details>',
          resolve:{
             "check":function($location, $http){this.check_token($location, $http)}
         }
    })
    .when("/login", {
         template : "<login></login>"
    })
    .when("/registration", {
         template : "<registration></registration>"
    })
    .otherwise('/');
});
