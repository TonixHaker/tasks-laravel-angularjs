registerModule.component("login",{
	templateUrl:"../templates/login.template.html",

	controller:function($http,$location) {
    this.user = {};
      this.check_token = ()=>{
        let token = localStorage.getItem('UserToken');
        if(token!=null){
          $http({
            method: 'POST',
            url: 'http://127.0.0.1:8000/api/auth/me',
            headers: {
               'Authorization': 'Bearer '+token
            },
          }).then(function successCallback(response) {
              //alert(response.data.name);
              $location.path('/');
            }, function errorCallback(response) {
              localStorage.removeItem('UserToken');
            });
        }
      }
      this.check_token();

      this.try_login = function(){
        console.log(this.user.email+this.user.password);
        $http({
          method: 'POST',
          url: 'http://127.0.0.1:8000/api/auth/login',
          params: {email: this.user.email, password: this.user.password},
        }).then(function successCallback(response) {
            localStorage.setItem('UserToken',response.data.access_token);
            $location.path('/');
          }, function errorCallback(response) {
            localStorage.removeItem('UserToken');
          });
      }
    }


});
