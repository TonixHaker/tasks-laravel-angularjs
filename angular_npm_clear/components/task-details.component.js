taskApp.component("taskDetails",{
	template:`
		<h1>{{$ctrl.task.name}}</h1>
		<p>{{$ctrl.task.description}}</p>
		<a href="#!/" class="btn btn-info">Menu</a>
	`,

	controller: ['$routeParams',
      function PhoneDetailController($routeParams) {
        this.id = $routeParams.taskId;
        this.task = tasks.find(task => {return this.id === task.id+""});

      }
    ]

});