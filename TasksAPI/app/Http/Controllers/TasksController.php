<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TasksController extends Controller
{
    function get_user_tasks(){
        $user = auth()->user();
        if(!$user){
            return response()
                ->json(['error' => 'Access denied']);
        }
        return response()
            ->json(['tasks' => 'Some tasks']);
    }
}
